package docker

import "time"
import "runtime"

const DockerAPIVersion = "1.24"
const dockerLabelPrefix = "com.gitlab.gitlab-runner"

func prebuiltImageName( ) string {
    if runtime.GOOS == "windows" {
        return "gitlab/gitlab-runner-helper-windows"
    } else {
        return "gitlab/gitlab-runner-helper"
    }
}
const prebuiltImageExtension = ".tar.xz"

const dockerCleanupTimeout = 5 * time.Minute
