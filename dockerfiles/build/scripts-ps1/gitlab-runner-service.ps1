
host=$DOCKER_PORT_6379_TCP_ADDR
port=$DOCKER_PORT_6379_TCP_PORT

if (( $host -eq $null ) || ( $port -eq $null )) {
    echo "No HOST or PORT"
    sys.exit(1)
}

echo -n "waiting for TCP connection to $host:$port..."

while ( $true ) {
    $success = nc -w 1 $host $port
    if ( $success -eq 0 ) {
        break
    }
    echo -n '.'
    sleep 1
}

echo 'ok'